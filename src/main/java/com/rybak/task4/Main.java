package com.rybak.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
    Scanner scanner = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Task4 task4 = new Task4();
        Main main = new Main();
        String text = main.readFromConsole();

        long l = task4.countUniqueWords(text);
       log.info("Unique words in the text= "+l);

        List<String> sortedList = task4.uniqueWordsSorted(text);
        log.info("Sorted list of words:");
        sortedList.forEach(System.out::println);

        Map<String, Integer> mapWords = task4.countOccurrenceOfWords(text);
        log.info("Occurrence number of each word in the text:");
        log.info(mapWords);


        Map<String, Long> mapSymbols = task4.countOccurrenceOfSymbols(text);
        log.info("Occurrence number of each symbol in the text:");
        log.info(mapSymbols);
    }


    public String readFromConsole() {
        String text = "";
        StringBuilder readerResult = new StringBuilder();
        System.out.println("Please, enter your text: ");
        String readLine;
        do {
            readLine = scanner.nextLine();
            readerResult.append(readLine).append("\n");
        } while (!readLine.equals(""));
        text = readerResult.toString();
        return text;
    }
}

