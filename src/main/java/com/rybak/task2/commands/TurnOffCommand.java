package com.rybak.task2.commands;

import com.rybak.task2.model.Device;

public class TurnOffCommand implements Command {

    private Device device;

    public TurnOffCommand(Device device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.turnOff();
    }


}
