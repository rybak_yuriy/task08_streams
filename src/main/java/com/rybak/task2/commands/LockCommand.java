package com.rybak.task2.commands;

import com.rybak.task2.model.Device;

public class LockCommand implements Command {

    Device device;

    public LockCommand(Device device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.lock();
    }

    @Override
    public String toString() {
        return "LockCommand{" +
                "device=" + device +
                '}';
    }
}
