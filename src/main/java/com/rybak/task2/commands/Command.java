package com.rybak.task2.commands;

public interface Command {

    void execute();
}
