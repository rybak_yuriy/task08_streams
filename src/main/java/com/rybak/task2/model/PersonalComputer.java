package com.rybak.task2.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PersonalComputer extends Device {

    private static Logger log = LogManager.getLogger(PersonalComputer.class);

    public void turnOn() {
        log.info("PC is turnOn");
    }

    public String turnOff() {
        log.info("PC is turnOff");
        return "PC is turnOff";
    }

    public void restart() {
        log.info("PC is restart");
    }

    public void lock() {
        log.info("PC is locked");
    }

    @Override
    public String toString() {
        return "PersonalComputer{}";
    }
}
